import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

export default defineConfig({
  build: {
    assetsDir: "",
    manifest: true,
    rollupOptions: {
      output: {
        assetFileNames: (chunkInfo) => {
          const extantion = chunkInfo.name?.split(".").pop();

          switch (extantion) {
            case "svg":
            case "png":
              return `assets/images/[name].[hash].[ext]`;
            case "css":
              return `assets/stylesheets/[name].[hash].[ext]`;
            default:
              return `assets/[name].[hash].[ext]`;
          }
        },
        chunkFileNames: (chunkInfo) => {
          if (!chunkInfo.isDynamicEntry) {
            return `export.[hash].js`;
          }

          const path = [
            chunkInfo?.facadeModuleId?.split("/").slice(-2)[0],
            `${chunkInfo.name.toLowerCase()}.[hash].js`,
          ];
          return path.join("/");
        },
        compact: true,
      },
    },
  },
  json: {
    stringify: true,
  },
  plugins: [vue()],
  resolve: {
    alias: {
      "~": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
});

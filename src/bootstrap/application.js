import { createApp, h, resolveComponent } from "vue";
import { createRouter } from "./router.js";

import "~/resources/stylesheets/application.css";

const application = createApp({
  name: "IrinaPortfolio",
  render: () => h(resolveComponent("RouterView")),
});

const router = createRouter();

application.use(router).mount("#application");

import { createRouter as _createRouter, createWebHistory } from "vue-router";
import routes from "~/routes";

export const createRouter = () => {
  return _createRouter({
    history: createWebHistory(),
    routes: [...routes],
    linkActiveClass: "",
    linkExactActiveClass: "",
  });
};
